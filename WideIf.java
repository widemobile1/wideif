import cdr.CallDetailRecord;
import cdr.CdrManagerHrs;
import cdr.CdrManagerWide;
import common.Configuration;
import common.WideUtil;
import db.DbManagerMssql;
import db.DbManagerOracle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;

public class WideIf
{
  private Logger logger = Logger.getLogger(getClass());
  private Configuration config = new Configuration();
  private CdrManagerHrs cdrMgrHrs = new CdrManagerHrs();
  private CdrManagerWide cdrMgrWide = new CdrManagerWide();
  private DbManagerOracle dbMgrHrs = new DbManagerOracle();
  private DbManagerMssql dbMgrWide = new DbManagerMssql();

  private boolean init() throws Exception {
    boolean initResult = false;

    this.logger.info("설정 파일 읽기를 시작합니다.");
    if (!this.config.read()) {
      this.logger.error("설정 파일 읽기에 실패했습니다. 로그를 참고하여 설정파일의 위치, 내용을 확인하세요.");
      return initResult;
    }

    this.logger.info("HRS DB 연결을 시작합니다.");
    this.dbMgrHrs.setURL(this.config.getConfig("db.hrs.url"));
    this.dbMgrHrs.setUserName(this.config.getConfig("db.hrs.user"));
    this.dbMgrHrs.setPassword(this.config.getConfig("db.hrs.password"));
    if (!this.dbMgrHrs.setConnection()) {
      this.logger.error("HRS DB 연결에 실패했습니다. 네트워크 상태나 설정파일의 DB 접속 정보를 확인하세요.");
      return initResult;
    }
    this.logger.info("HRS DB 연결에 성공했습니다.");

    this.logger.info("WIDE DB 연결을 시작합니다.");
    this.dbMgrWide.setURL(this.config.getConfig("db.wide.url"));
    this.dbMgrWide.setUserName(this.config.getConfig("db.wide.user"));
    this.dbMgrWide.setPassword(this.config.getConfig("db.wide.password"));
    if (!this.dbMgrWide.setConnection()) {
      this.logger.error("WIDE DB 연결에 실패했습니다.");
      return initResult;
    }
    this.logger.info("WIDE DB 연결에 성공했습니다.");

    return initResult = true;
  }

  private void run() {
    this.logger.info("Main 프로세스가 시작됩니다.");
    CallDetailRecord cdrWide = null;
    CallDetailRecord cdrHrs = null;
    while (true)
    {
      this.logger.info("전송 대상 CDR를 조회합니다.");
      List cdrList = new ArrayList();
      try {
        cdrList = this.dbMgrHrs.querySelectList(this.cdrMgrHrs.querySelectCDRList());
      }
      catch (Exception e) {
        e.printStackTrace();
      }

      for (int i = 0; i < cdrList.size(); i++) {
        cdrWide = new CallDetailRecord();
        cdrHrs = new CallDetailRecord();
        cdrWide.importCdr((HashMap)cdrList.get(i));
        cdrHrs.importCdr((HashMap)cdrList.get(i));
        try
        {
          if ((this.dbMgrWide.queryInsert(this.cdrMgrWide.queryInsertCdr(cdrWide))) && 
            (this.dbMgrHrs.queryUpate(this.cdrMgrHrs.queryUpdateStatus(cdrHrs, "FIN")))) {
            this.logger.info("CDR Process complete : " + cdrHrs.seqno + ",(" + cdrHrs.seqno + ")");
          } else {
            this.dbMgrHrs.rollback();
            this.dbMgrWide.rollback();
            this.dbMgrHrs.queryUpate(this.cdrMgrHrs.queryUpdateStatus(cdrHrs, "ERR"));
          }
          this.dbMgrHrs.commit();
          this.dbMgrWide.commit();
        } catch (Exception e) {
          this.dbMgrHrs.rollback();
          this.dbMgrWide.rollback();
        }

        if (!this.dbMgrWide.getStatus()) {
          try {
            this.dbMgrWide.setConnection();
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
        if (this.dbMgrHrs.getStatus()) continue;
        try {
          this.dbMgrHrs.setConnection();
        } catch (Exception e) {
          e.printStackTrace();
        }

      }

      try
      {
        this.logger.info("5 seconds sleep");
        WideUtil.sleep(5);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  public void shutdown() {
    try {
      this.dbMgrWide.close();
      this.dbMgrHrs.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
    this.logger.info("프로그램을 종료합니다.");
  }

  public static void main(String[] args) {
    WideIf wideIf = new WideIf();
    try
    {
      if (wideIf.init()) wideIf.run(); 
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    wideIf.shutdown();
    System.exit(0);
  }
  }
