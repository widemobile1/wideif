package cdr;

import java.util.HashMap;

public class CallDetailRecord
{
  public long seqno = 0L;
  public String partner_id = "";
  public String acct_id = "";
  public String area_prefix = "";
  public String min_no = "";
  public String call_no = "";
  public String start_time = "";
  public String end_time = "";
  public String proc_status = "";
  public String bill_cd = "";
  public double cost_amount = 0.0D;
  public double sales_amount = 0.0D;
  public long tax = 0L;
  public double total_amount = 0.0D;
  public String usage_unit = "";
  public String usage_type = "";
  public long usage_amount = 0L;
  public String ext_seqno = "";
  public String err_msg = "";
  public String area_name = "";
  public String country = "";
  public String countryId = "";
  public int duration = 0;

  public int getDuration() {
    return this.duration;
  }

  public void setDuration(int duration) {
    this.duration = duration;
  }

  public void importCdr(HashMap<String, String> rawCdr) {
    if (rawCdr.get("SEQNO") != null) this.seqno = Long.parseLong((String)rawCdr.get("SEQNO"));
    if (rawCdr.get("PARTNER_ID") != null) this.partner_id = ((String)rawCdr.get("PARTNER_ID"));
    if (rawCdr.get("ACCT_ID") != null) this.acct_id = ((String)rawCdr.get("ACCT_ID"));
    if (rawCdr.get("AREA_PREFIX") != null) this.area_prefix = ((String)rawCdr.get("AREA_PREFIX"));
    if (rawCdr.get("MIN_NO") != null) this.min_no = ((String)rawCdr.get("MIN_NO"));
    if (rawCdr.get("CALL_NO") != null) this.call_no = ((String)rawCdr.get("CALL_NO"));
    if (rawCdr.get("START_TIME") != null) this.start_time = ((String)rawCdr.get("START_TIME"));
    if (rawCdr.get("END_TIME") != null) this.end_time = ((String)rawCdr.get("END_TIME"));
    if (rawCdr.get("PROC_STATUS") != null) this.proc_status = ((String)rawCdr.get("PROC_STATUS"));
    if (rawCdr.get("BILL_CD") != null) this.bill_cd = ((String)rawCdr.get("BILL_CD"));
    if (rawCdr.get("COST_AMOUNT") != null) this.cost_amount = Double.parseDouble((String)rawCdr.get("COST_AMOUNT"));
    if (rawCdr.get("SALES_AMOUNT") != null) this.sales_amount = Double.parseDouble((String)rawCdr.get("SALES_AMOUNT"));
    if (rawCdr.get("TAX") != null) this.tax = Long.parseLong((String)rawCdr.get("TAX"));
    if (rawCdr.get("TOTAL_AMOUNT") != null) this.total_amount = Double.parseDouble((String)rawCdr.get("TOTAL_AMOUNT"));
    if (rawCdr.get("USAGE_UNIT") != null) this.usage_unit = ((String)rawCdr.get("USAGE_UNIT"));
    if (rawCdr.get("USAGE_TYPE") != null) this.usage_type = ((String)rawCdr.get("USAGE_TYPE")).trim();
    if (rawCdr.get("USAGE_AMOUNT") != null) this.usage_amount = Long.parseLong((String)rawCdr.get("USAGE_AMOUNT"));
    if (rawCdr.get("EXT_SEQNO") != null) this.ext_seqno = ((String)rawCdr.get("EXT_SEQNO"));
    if (rawCdr.get("ERR_MSG") != null) this.err_msg = ((String)rawCdr.get("ERR_MSG"));
    if (rawCdr.get("AREA_NAME") != null) this.area_name = ((String)rawCdr.get("AREA_NAME"));
    if (rawCdr.get("COUNTRY") != null) this.country = ((String)rawCdr.get("COUNTRY"));
    if (rawCdr.get("COUNTRYID") != null) this.countryId = ((String)rawCdr.get("COUNTRYID"));
    if (rawCdr.get("DURATION") != null) this.duration = Integer.parseInt((String)rawCdr.get("DURATION"));
  }
}
