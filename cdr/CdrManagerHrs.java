package cdr;

public class CdrManagerHrs
{
  public String querySelectCDRList()
    throws Exception
  {
    String query = "SELECT * FROM ( "
		    		+ "SELECT SEQNO, PARTNER_ID, ACCT_ID, AREA_PREFIX, MIN_NO, CALL_NO, START_TIME, END_TIME, "
		    		+ "PROC_STATUS, BILL_CODE, COST_AMOUNT, SALES_AMOUNT, TAX, TOTAL_AMOUNT, USAGE_UNIT, UPPER(USAGE_TYPE) AS USAGE_TYPE, "
		    		+ "USAGE_AMOUNT, EXT_SEQNO, ERR_MSG, COUNTRY.COUNTRY_CODE as COUNTRYID, COUNTRY.COUNTRY_NAME as COUNTRY, "
		    		+ "( SELECT CEIL(USAGE_RATED.USAGE_AMOUNT/RATE.SALES_UNIT) "
			    		+ "FROM PARTNER_RATE RATE "
			    		+ "WHERE RATE.BILL_CODE = USAGE_RATED.BILL_CODE AND rate.partner_id = usage_rated.partner_id) "
			    		+ "AS DURATION    "
		    		+ "FROM usage_rated LEFT OUTER JOIN country ON usage_rated.area_prefix = country.country_prefix "
		    		+ "WHERE PROC_STATUS = 'INS'  AND USAGE_RATED.CRT_DT > SYSDATE - 10     AND USAGE_RATED.START_TIME > TO_DATE('20151101','YYYYMMDD')   AND USAGE_RATED.TOTAL_AMOUNT > 0 "
		    		+ "AND usage_rated.partner_id in (select partner_id from partner_info where partner_root ='2')" // rms만 분리. 나중에 코드화 해야됨
		    		+ " ORDER BY SEQNO ASC  "
		    		+ ") WHERE ROWNUM <= 101";

    return query;
  }

  public String queryUpdateStatus(CallDetailRecord cdr, String status) throws Exception {
    String query = "UPDATE USAGE_RATED   SET PROC_STATUS = '" + 
      status + "'" + 
      "      ,UPDT_USER = '" + "WIDEIF" + "'" + 
      "      ,UPDT_DT = SYSDATE" + 
      " WHERE SEQNO = " + cdr.seqno;

    return query;
  }
}
