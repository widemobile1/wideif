package cdr;

import org.apache.log4j.Logger;

public class CdrManagerWide
{
  private Logger logger = Logger.getLogger(getClass());

  public String queryInsertCdr(CallDetailRecord cdr) throws Exception {
    int duration = 0;
    int seconds = 0;
    int callDesc = 0;

    duration = cdr.duration;
    seconds = (int)cdr.usage_amount;

    this.logger.info("cdr info : [country][countryId][usage_type]=[" + cdr.country + "][" + cdr.countryId + "][" + cdr.usage_type + "]");

    if ((cdr.country != null) && (cdr.countryId.equals("5"))) {
      if (cdr.usage_type.equals("VOICE")) {
        callDesc = 27;
        this.logger.info("cdr info : [callDescription]=[" + callDesc + "][1]");
      } else if (cdr.usage_type.equals("SMS")) {
        callDesc = 13;
        this.logger.info("cdr info : [callDescription]=[" + callDesc + "][2]");
      } else if (cdr.usage_type.equals("MMS")) {
        callDesc = 57;
        this.logger.info("cdr info : [callDescription]=[" + callDesc + "][3]");
      } else if (cdr.usage_type.equals("VIDEO")) {
        callDesc = 58;
        this.logger.info("cdr info : [callDescription]=[" + callDesc + "][4]");
      } else if (cdr.usage_type.equals("DATA")) {
        callDesc = 51;
        this.logger.info("cdr info : [callDescription]=[" + callDesc + "][5]");
        if (cdr.total_amount < 256000.0D)
          cdr.duration = 0;
      }
    }
    else if (cdr.country != null) {
      if (cdr.usage_type.equals("SMS")) {
        callDesc = 26;
        this.logger.info("cdr info : [callDescription]=[" + callDesc + "][6]");
      } else if (cdr.call_no.startsWith("005")) {
        callDesc = 9;
        this.logger.info("cdr info : [callDescription]=[" + callDesc + "][7]");
      } else {
        callDesc = 35;
        this.logger.info("cdr info : [callDescription]=[" + callDesc + "][8]");
      }
    } else {
      callDesc = 0;
      this.logger.info("cdr info : [callDescription]=[" + callDesc + "][9]");
    }

    String query = "INSERT INTO TelsChargeCdr (NationID, PhoneNo, CallTime, Duration,  Seconds, DialedNo, CountryID, CallDescription, Bzonenm, Ext_seqno) VALUES ( 5,'" + 
      cdr.min_no + "'," + 
      "convert(datetime,'" + cdr.start_time + "'),'" + 
      duration + "','" + 
      seconds + "','" + 
      cdr.call_no + "','" + 
      cdr.countryId + "','" + 
      callDesc + "','" + 
      cdr.country + "','" + 
      cdr.seqno + "')";

    return query;
  }
}
