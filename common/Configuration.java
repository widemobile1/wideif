 package common;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import org.apache.log4j.Logger;

public class Configuration
{
  private Logger logger = Logger.getLogger(getClass());
  Map<String, String> configMap = new HashMap();

  public boolean read() {
    boolean readResult = false;

    this.logger.info("Configuration file read : [START] ");
    String config = "";
    String configTemp = "";
    String configKeyTemp = "";
    String configValTemp = "";
    try
    {
      this.logger.info(getClass().getClassLoader().getResource("wideif_config.properties").getPath());
      File conf_file = new File(getClass().getClassLoader().getResource("wideif_config.properties").getPath());
      InputStream fis = null;
      fis = new FileInputStream(conf_file);
      byte[] b = new byte[1];

      while (fis.read(b) != -1) {
        config = config + new String(b);
      }

      readResult = true;
      fis.close();
    } catch (Exception e) {
      this.logger.error("Configuration file read : [FAIL] " + e);
    }

    this.logger.debug("Configuration file parsing : [START]");
    this.logger.info("+============================================================+");
    this.logger.info("|                    Configuraion List                       |");
    this.logger.info("+============================================================+");
    try {
      StringTokenizer st = new StringTokenizer(config, ";");
      while (st.hasMoreTokens()) {
        configTemp = st.nextToken().trim();
        if (configTemp.length() != 0) {
          configKeyTemp = configTemp.substring(0, configTemp.indexOf("="));
          configValTemp = configTemp.substring(configTemp.indexOf("=") + 1, configTemp.length());
          this.configMap.put(configKeyTemp, configValTemp);
          if (!configKeyTemp.equals("password"))
            this.logger.info(configKeyTemp + ":[" + configValTemp + "]");
        }
      }
    }
    catch (Exception e) {
      this.logger.error("Configuration file parsing : [FAIL] " + e);
    }

    this.logger.info("==============================================================");
    this.logger.info("Configuration file read and parsing : [OK]");

    return readResult;
  }

  public String getConfig(String tempKey) {
    this.logger.debug("read config (key : " + tempKey + ", value : " + (String)this.configMap.get(tempKey) + ")");
    return (String)this.configMap.get(tempKey);
  }
}
