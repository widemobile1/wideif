package common;

public class WideConstraints
{
  public static final String CONFIG_DB_RETRY_MAX_COUNT = "db.retry.max.count";
  public static final String CONFIG_DB_HRS_URL = "db.hrs.url";
  public static final String CONFIG_DB_HRS_USER = "db.hrs.user";
  public static final String CONFIG_DB_HRS_PASSWORD = "db.hrs.password";
  public static final String CONFIG_DB_WIDE_URL = "db.wide.url";
  public static final String CONFIG_DB_WIDE_USER = "db.wide.user";
  public static final String CONFIG_DB_WIDE_PASSWORD = "db.wide.password";
  public static final String CRT_USER = "WIDEIF";
  public static final String PROC_STATUS_INS = "INS";
  public static final String PROC_STATUS_FIN = "FIN";
  public static final String PROC_STATUS_ERR = "ERR";
  public static final String NATION_ID_LOCAL = "5";
  public static final String CALL_TYPE_LOCAL = "5";
  public static final String USAGE_TYPE_DATA = "DATA";
  public static final String USAGE_TYPE_SMS = "SMS";
  public static final String USAGE_TYPE_MMS = "MMS";
  public static final String USAGE_TYPE_VOICE = "VOICE";
  public static final String USAGE_TYPE_VIDEO = "VIDEO";
  public static final int WIDE_CALL_DESC_LOCAL_CALL = 27;
  public static final int WIDE_CALL_DESC_LOCAL_SMS = 13;
  public static final int WIDE_CALL_DESC_LOCAL_MMS = 57;
  public static final int WIDE_CALL_DESC_LOCAL_VIDEO = 58;
  public static final int WIDE_CALL_DESC_INT_SMS = 26;
  public static final int WIDE_CALL_DESC_INT_005 = 9;
  public static final int WIDE_CALL_DESC_INT_NO_005 = 35;
  public static final int WIDE_CALL_DESC_LOCAL_DATA = 51;
}