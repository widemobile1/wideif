package db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.log4j.Logger;

public abstract class DBManager
{
  Logger logger = Logger.getLogger(getClass());
  String url;
  String user;
  String password;
  boolean status;
  int dbRetryMaxCount = 3;
  Connection conn = null;

  public abstract boolean setConnection()
    throws Exception;

  public boolean getStatus()
  {
    return this.status;
  }

  public void setStatus(boolean status) {
    this.status = status;
  }

  public void setUserName(String user) {
    this.user = user;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public void setURL(String url) {
    this.url = url;
  }

  public void setRetryMaxCount(int dbRetryMaxCount) {
    this.dbRetryMaxCount = dbRetryMaxCount;
  }

  public void commit() throws Exception {
    try {
      this.conn.commit();
      this.logger.debug("Execute commit : [OK]");
    } catch (SQLException e) {
      this.logger.error("Execute commit : [FAIL] " + e);
    }
  }

  public void rollback() {
    try {
      this.conn.rollback();
      this.logger.error("Execute rollback : [OK]");
    } catch (SQLException e) {
      this.logger.error("Execute commit : [FAIL] " + e);
    }
  }

  public void close() {
    try {
      this.conn.close();
      this.logger.info("DB Connection close : [OK]");
    } catch (SQLException e) {
      this.logger.error("DB Connection close : [FAIL] " + e);
    }
  }

  public boolean queryInsert(String query) {
    boolean result = false;
    int resultCount = 0;
    Statement stmt = null;
    try
    {
      this.logger.debug("[queryInsert] " + query);
      stmt = this.conn.createStatement();
      resultCount = stmt.executeUpdate(query);
      if (resultCount > 0) {
        this.logger.info("Execute query [Insert] : [OK], Insert #:" + resultCount);
        result = true;
      }
      stmt.close();
    } catch (Exception e) {
      this.logger.error("Execute query [Insert] : [FAIL] " + e);
      this.status = false;
      try {
        stmt.close(); } catch (Exception e1) { this.logger.error("Statement Closing Error"); }  } finally {
      try { stmt.close(); } catch (Exception e) { this.logger.error("Statement Closing Error");
      }
    }
    return result;
  }

  public String querySelect(String query)
    throws Exception
  {
    String result = "";
    ResultSet rs = null;
    Statement stmt = null;
    try
    {
      this.logger.debug("[querySelect] " + query);
      stmt = this.conn.createStatement();
      rs = stmt.executeQuery(query);
      if (rs.next()) {
        result = rs.getString(1);
      }
      this.logger.info("Execute query [Select] : [OK], result : [" + result + "]");
      rs.close();
      stmt.close();
    } catch (Exception e) {
      this.logger.error("Execute query [Select] : [FAIL] " + e);
      this.status = false;
      throw e; } finally {
      try {
        stmt.close(); } catch (Exception e) { this.logger.error("Statement Closing Error");
      }
    }
    return result;
  }

  public List<HashMap<String, String>> querySelectList(String query)
    throws Exception
  {
    List resultMapList = new ArrayList();
    HashMap resultMap = new HashMap();
    ResultSet rs = null;
    ResultSetMetaData rsmd = null;
    Statement stmt = null;
    try
    {
      this.logger.debug("[querySelectList] " + query);
      stmt = this.conn.createStatement();
      rs = stmt.executeQuery(query);
      rsmd = rs.getMetaData();
      int colCount = rsmd.getColumnCount();
      int cnt = 0;

      while (rs.next()) {
        resultMap = new HashMap();
        for (int i = 1; i <= colCount; i++) {
          resultMap.put(rsmd.getColumnName(i), rs.getString(i));
        }
        resultMapList.add(resultMap);
        cnt++;
      }

      this.logger.info("Execute query [SelectList] : [OK], Selected # : [" + cnt + "]");
      rs.close();
      stmt.close();
    } catch (Exception e) {
      this.logger.error("Execute query [SelectList] : [FAIL] " + e);
      this.status = false;
      throw e; } finally {
      try {
        stmt.close(); } catch (Exception e) { this.logger.error("Statement Closing Error");
      }
    }
    return resultMapList;
  }

  public boolean queryUpate(String query) throws Exception {
    boolean result = false;
    int resultCount = 0;
    Statement stmt = null;
    try
    {
      this.logger.debug("[queryUpdate] " + query);
      stmt = this.conn.createStatement();
      resultCount = stmt.executeUpdate(query);
      if (resultCount > 0) {
        this.logger.info("Execute query [Update] : [OK], Update # : [" + resultCount + "]");
        result = true;
      }
      stmt.close();
    } catch (Exception e) {
      this.logger.error("Execute query [Update] : [FAIL] " + e);
      this.status = false;
      try {
        stmt.close(); } catch (Exception e1) { this.logger.error("Statement Closing Error"); }  } finally {
      try { stmt.close(); } catch (Exception e) { this.logger.error("Statement Closing Error"); }
    }
    return result;
  }

  public boolean queryDelete(String query) throws Exception {
    boolean result = false;
    int resultCount = 0;
    Statement stmt = null;
    try
    {
      this.logger.debug("[queryDelete] " + query);
      stmt = this.conn.createStatement();
      resultCount = stmt.executeUpdate(query);
      if (resultCount > 0) {
        this.logger.info("Execute query [Delete] : [OK], Delete # : [" + resultCount + "]");
        result = true;
      }
      stmt.close();
    } catch (Exception e) {
      this.logger.error("Execute query [Delete] : [FAIL] " + e);
      this.status = false;
      try {
        stmt.close(); } catch (Exception e1) { this.logger.error("Statement Closing Error"); }  } finally {
      try { stmt.close(); } catch (Exception e) { this.logger.error("Statement Closing Error"); }
    }
    return result;
  }
}
