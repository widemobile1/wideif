package db;

import common.WideUtil;
import java.sql.Connection;
import java.sql.DriverManager;
import org.apache.log4j.Logger;

public class DbManagerMssql extends DBManager
{
  public boolean setConnection()
    throws Exception
  {
    for (int i = 1; i <= this.dbRetryMaxCount; i++ ) {
      try {
        if (this.conn != null) {
          this.conn.close();
        }
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        this.conn = DriverManager.getConnection(this.url, this.user, this.password);
        this.conn.setAutoCommit(false);
        this.logger.info("DB Connection sucess.");
        this.status = true;
      }
      catch (Exception e) {
        this.logger.error("Source DB 연결 실패 : " + i + "번째");
        this.logger.error("DB Connection fail : " + e);
        WideUtil.sleep(10);
      }

    }

    return this.status;
  }
}
